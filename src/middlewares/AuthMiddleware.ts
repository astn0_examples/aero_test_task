import { Request, Response, NextFunction } from 'express'
import IRequest from '../controllers/IRequest'
import IProfile from '../domains/ProfileDomain/IProfile'
import ProfileDomain from '../domains/ProfileDomain/ProfileDomain'
import TokensHelper from '../helpers/TokensHelper'
import Config from '../globals/Config'
export default class AuthMiddleware {
  public static async doAuth(
    req: IRequest,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    // todo: add ProfileDomain
    try {
      let token = req.headers.authorization as string
      if (token.search(/^[Bb]earer/) === -1) throw new Error('401')
      token = token.split(' ')[1].trim()
      if (!token) throw new Error('401')
      // todo: проверяем хелпером

      const tokensHelper = new TokensHelper(
        Config.tokens.salt,
        Config.tokens.accessLifeTime,
        Config.tokens.refreshLifeTime
      )
      const payload = await tokensHelper.checkToken(token)
      const profileDomain = new ProfileDomain()
      const profile: IProfile = await profileDomain.getProfileByToken(token)
      if (!profile) throw new Error('401')
      if (payload.userID !== profile.userID) throw new Error('401')
      req.userID = profile.userID
    } catch (err) {
      throw new Error('401')
    }

    next()
  }
}
