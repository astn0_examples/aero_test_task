import amqp from 'amqplib/callback_api'
export default class QueueService {
  public static async sendMessage(queue: string, mess: string) {
    console.log('Отправляем ссобщаку в ребит')
    amqp.connect('amqp://rabbitmq', (error0, connection) => {
      if (error0) {
        throw error0
      }
      connection.createChannel((error1, channel) => {
        if (error1) {
          throw error1
        }

        channel.assertQueue(queue, {
          durable: false,
        })

        channel.sendToQueue(queue, Buffer.from(mess))
        console.log(' [x] Sent %s', mess)
      })
    })
  }
}
