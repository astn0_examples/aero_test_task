import TokensHelper from '../helpers/TokensHelper'
test('Создаем токены', async () => {
  const secret = 'verysecretKey'
  const userID = 'user-test'

  const accessLifeTime = 1000
  const refreshLifeTime = 2000

  const tokenHelper = new TokensHelper(secret, accessLifeTime, refreshLifeTime)
  const wrongTokenHelper = new TokensHelper(
    'rh23r23rh23rh',
    accessLifeTime,
    refreshLifeTime
  )
  const { token, refreshToken } = tokenHelper.createTokens(userID)

  let tokenPayload = TokensHelper.getPayload(token)
  expect(userID).toBe(tokenPayload.userID)
  expect('accessToken').toBe(tokenPayload.type)

  tokenPayload = TokensHelper.getPayload(refreshToken)
  expect(userID).toBe(tokenPayload.userID)
  expect('refreshToken').toBe(tokenPayload.type)

  let nullResult = TokensHelper.getPayload('38h23fh23fh23fhfjvd')
  expect(nullResult).toBe(null)

  let checkTokenPayload = tokenHelper.checkToken(token)
  expect(userID).toBe(checkTokenPayload.userID)
  expect('accessToken').toBe(checkTokenPayload.type)

  try {
    let nullPayload = tokenHelper.checkToken('sdsfe9hw9fwwisfl')
    expect(false).toBe(true)
  } catch (err) {
    expect(true).toBe(true)
  }

  try {
    let nullPayload = wrongTokenHelper.checkToken('sdsfe9hw9fwwisfl')
    expect(false).toBe(true)
  } catch (err) {
    expect(true).toBe(true)
  }

  checkTokenPayload = tokenHelper.checkToken(refreshToken)
  expect(userID).toBe(checkTokenPayload.userID)
  expect('refreshToken').toBe(checkTokenPayload.type)

  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, accessLifeTime)
  })

  try {
    checkTokenPayload = tokenHelper.checkToken(token)
  } catch (err) {
    console.log('Токен умер')
    expect(true).toBe(true)
  }

  checkTokenPayload = tokenHelper.checkToken(refreshToken)
  expect(userID).toBe(checkTokenPayload.userID)
  expect('refreshToken').toBe(checkTokenPayload.type)

  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, refreshLifeTime)
  })

  try {
    checkTokenPayload = tokenHelper.checkToken(refreshToken)
  } catch (err) {
    console.log('refresh Токен умер')
    expect(true).toBe(true)
  }
})
