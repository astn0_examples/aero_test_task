export default interface IPagination {
  /**
   * Предыдущая страница
   */
  hasPrevPage: boolean
  /**
   * Следующая страница
   */
  hasNextPage: boolean
  /**
   * Номер предыдущей страницы
   */
  prevPage: number | null
  /**
   * Номер следующей страницы
   */
  nextPage: number | null
  /**
   * Данных на странице
   */
  list_size: number // perPage: number
  /**
   * Текущая страница
   */
  page?: number | null
  /**
   * Всего старниц
   */
  totalPages?: number
}
