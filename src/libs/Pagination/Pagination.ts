export default class Pagination {
  /**
   * Пагинация
   *
   * @param paginationParams
   * @param targetArray
   * @private
   */
  public static pagination(
    page: number,
    list_size: number,
    targetArray: Array<any>
  ) {
    if (!page || !list_size) {
      return {
        data: targetArray,
        pagination: {
          hasPrevPage: false,
          hasNextPage: false,
          prevPage: 0,
          nextPage: 0,
          list_size: targetArray.length,
          page: 1,
          totalPages: 1,
        },
      }
    }
    // колличество секций
    let totalReal = targetArray.length / list_size
    let totalPages =
      Math.trunc(totalReal) < totalReal ? Math.trunc(totalReal) + 1 : totalReal
    let hasNextPage = false
    let hasPrevPage = false

    // если ввели номер не существующей страницы, то заменим его на последний существующий
    if (totalPages < page) page = totalPages

    let rightIndex = 0
    for (let i = 0; i !== page; i++) {
      rightIndex += list_size
    }
    let leftIndex = rightIndex - list_size

    if (leftIndex !== 0) hasPrevPage = true
    if (rightIndex < targetArray.length) hasNextPage = true

    let data: Array<any> = targetArray.slice(leftIndex, rightIndex)

    return {
      data,
      pagination: {
        page,
        list_size,
        hasNextPage,
        hasPrevPage,
        prevPage: hasPrevPage ? page - 1 : 0,
        nextPage: hasNextPage ? page + 1 : 0,
        totalPages,
      },
    }
  }
}
