import { Sequelize } from 'sequelize'

// 'database', 'username', 'password'
/*
const SequelizeSingleton = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite',
})
*/
const SequelizeSingleton = new Sequelize('test', 'root', '12345', {
  dialect: 'mysql',
  host: 'db',
})

SequelizeSingleton.sync()

export default SequelizeSingleton
