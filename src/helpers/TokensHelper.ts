import jwt from 'jsonwebtoken'
enum tokenTypes {
  accessToken = 'accessToken',
  refreshToken = 'refreshToken',
}
interface IPayload {
  userID: string
  timeCreate: number
  type: string
}
/**
 * Класс для работы с токенами
 */
export default class TokensHelper {
  private readonly _privateKey: string
  private readonly _accessLifeTime: number
  private readonly _refreshLifeTime: number

  constructor(
    privateKey: string,
    accessLifeTime: number,
    refreshLifeTime: number
  ) {
    this._privateKey = privateKey
    this._accessLifeTime = accessLifeTime
    this._refreshLifeTime = refreshLifeTime
  }

  /**
   * Создаем токены access token и refresh token
   * @param userID
   */
  public createTokens(userID: string): { token: string; refreshToken: string } {
    const payload: IPayload = {
      userID,
      timeCreate: Date.now(),
      type: '',
    }

    const token = jwt.sign(
      { ...payload, type: tokenTypes.accessToken },
      this._privateKey
    )
    const refreshToken = jwt.sign(
      { ...payload, type: tokenTypes.refreshToken },
      this._privateKey
    )

    return {
      token,
      refreshToken,
    }
  }
  /**
   * Проверяем подпись для токена
   * @param token
   */
  public checkToken(token: string): IPayload {
    const payload: any = jwt.verify(token, this._privateKey)
    if (payload.type === tokenTypes.accessToken) {
      console.log('payload.timeCreate = ', payload.timeCreate)
      if (payload.timeCreate + this._accessLifeTime < Date.now())
        throw new Error('401')
      return payload
    } else if (payload.type === tokenTypes.refreshToken) {
      if (payload.timeCreate + this._refreshLifeTime < Date.now())
        throw new Error('401')
      return payload
    } else {
      throw new Error('401')
    }
  }

  /**
   * Получаем инфу из токена
   * @param token
   */
  public static getPayload(token: string): IPayload {
    const payload = jwt.decode(token) as IPayload
    return payload
  }
}
