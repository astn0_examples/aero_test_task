import IFiles from '../domains/FilesDomain/IFiles'

export default class FileMetaHelper {
  public getFileMeta(file: any): IFiles {
    const name: string = file.name
    const extension: string = this._getExtension(name)
    const path: string = this._generatePath() + '/' + name
    const size: number = parseInt(file.size)

    const uploadedFile: IFiles = {
      name: file.name,
      extension,
      mime: file.mimetype,
      size,
      uploadDate: new Date(),
      path,
    }

    return uploadedFile
  }

  private _generatePath(): string {
    return ''
  }

  private _getExtension(fileName: string): string {
    const filenameChanks: Array<string> = fileName.split('.')
    if (filenameChanks.length === 1) return ''
    return filenameChanks[filenameChanks.length - 1]
  }
}
