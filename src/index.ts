import cors from 'cors'
import bodyParser from 'body-parser'
import routes from './routes/Routes'
import errors from './controllers/Errors'
import express from 'express'
import { Request, Response, NextFunction } from 'express'
import fileUpload from 'express-fileupload'
import dotenv from 'dotenv'
dotenv.config()

const app: express.Application = express()

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(fileUpload())
app.use(cors())

app.use('/api', routes)

app.use((request: Request, response: Response, next: NextFunction) => {
  next(new Error('404'))
})
app.use(errors.ERROR)

app.listen(process.env.PORT, function () {
  console.log(`App is listening on port ${process.env.PORT}!`)
})
