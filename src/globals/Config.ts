interface IConfig {
  port: string
  store: string
  tokens: {
    salt: string
    accessLifeTime: number
    refreshLifeTime: number
  }
}

const Config: IConfig = {
  port: '8080',
  store: './uploads/',
  tokens: {
    salt: '43fh34ogh9ergj3Asdajg943gh394g',
    accessLifeTime: 600000,
    refreshLifeTime: 1000000,
  },
}
export default Config
