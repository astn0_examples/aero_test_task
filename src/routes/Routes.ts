import Router from 'express-promise-router'
import AuthController from '../controllers/AuthController'
import FilesController from '../controllers/FilesController'
import AuthMiddleware from '../middlewares/AuthMiddleware'

const router = Router()

router
  .route('/file/list')
  .get(AuthMiddleware.doAuth)
  .get(FilesController.getAllFiles)
router
  .route('/file/:id')
  .get(AuthMiddleware.doAuth)
  .get(FilesController.getOneFile)
router
  .route('/file/upload')
  .post(AuthMiddleware.doAuth)
  .post(FilesController.uploadOneFile)
router
  .route('/file/delete/:id')
  .delete(AuthMiddleware.doAuth)
  .delete(FilesController.deleteOneFile)
router
  .route('/file/update/:id')
  .put(AuthMiddleware.doAuth)
  .put(FilesController.updateOneFile)
router
  .route('/file/download/:id')
  .get(AuthMiddleware.doAuth)
  .get(FilesController.downloadOneFile)

router.route('/signup').post(AuthController.signup)
router.route('/signin').post(AuthController.signin)
router.route('/signin/new_token').post(AuthController.doRefreshToken)
router.route('/logout').get(AuthMiddleware.doAuth).get(AuthController.logout)

export default router
