import Config from '../../globals/Config'
import TokensHelper from '../../helpers/TokensHelper'
import ProfileModel from '../../models/ProfileModel'
import IProfile from './IProfile'
import LoginTypeEnum from './LofinTypeEnum'

export default class ProfileDomain {
  /**
   * Получаем профайл и обновляем для него токены
   * todo: Убери копипасту
   * @param login
   * @param password
   * @param loginType
   */
  public async getProfileByLogin(
    login: string,
    password: string,
    loginType: LoginTypeEnum
  ): Promise<IProfile> {
    const tokensHelper = new TokensHelper(
      Config.tokens.salt,
      Config.tokens.accessLifeTime,
      Config.tokens.refreshLifeTime
    )

    if (loginType === LoginTypeEnum.email) {
      const profile: IProfile = await ProfileModel.getProfileByEmail(login)

      // todo: пароль должен быть хешем в БД !
      if (password !== profile.password) throw new Error('401')

      const { token, refreshToken } = tokensHelper.createTokens(
        profile.userID as string
      )

      const profileWithTokens: IProfile = await ProfileModel.updateTokensByID(
        profile.userID as string,
        token,
        refreshToken
      )

      return profileWithTokens
    } else if (loginType === LoginTypeEnum.phone) {
      const profile: IProfile = await ProfileModel.getProfileByPhone(login)

      // todo: пароль должен быть хешем в БД !
      if (password !== profile.password) throw new Error('401')

      const { token, refreshToken } = tokensHelper.createTokens(
        profile.userID as string
      )

      const profileWithTokens: IProfile = await ProfileModel.updateTokensByID(
        profile.userID as string,
        token,
        refreshToken
      )

      return profileWithTokens
    } else {
      throw new Error('')
    }
  }
  /**
   * получаем профайл по токену (для проверки токена обычно)
   * @param token
   */
  public async getProfileByToken(token: string): Promise<IProfile> {
    const profile: IProfile = await ProfileModel.getProfileByToken(token)
    return profile
  }
  /**
   * Получаем профайл по refresh токену и обновляем токены
   * @param refToken
   */
  public async getProfileByRefreshToken(refToken: string): Promise<IProfile> {
    const profile: IProfile = await ProfileModel.getProfileByRefreshToken(
      refToken
    )
    const tokensHelper = new TokensHelper(
      Config.tokens.salt,
      Config.tokens.accessLifeTime,
      Config.tokens.refreshLifeTime
    )
    const { token, refreshToken } = tokensHelper.createTokens(
      profile.userID as string
    )
    const profileWithTokens: IProfile = await ProfileModel.updateTokensByID(
      profile.userID as string,
      token,
      refreshToken
    )
    return profileWithTokens
  }
  /**
   * обновляем токены по userID
   * @param profileID
   */
  public async getProfileByProfileID(profileID: string): Promise<IProfile> {
    const tokensHelper = new TokensHelper(
      Config.tokens.salt,
      Config.tokens.accessLifeTime,
      Config.tokens.refreshLifeTime
    )
    const { token, refreshToken } = tokensHelper.createTokens(
      profileID as string
    )
    const profile: IProfile = await ProfileModel.updateTokensByID(
      profileID,
      token,
      refreshToken
    )
    return profile
  }
  /**
   * Создаем профайл и пару токенов
   * @param newProfile
   */
  public async addProfile(newProfile: IProfile): Promise<IProfile> {
    /*
    try {
      const password = new Password(newProfile.password, '')
      newProfile.password = await password.generateHashPassword()
    } catch (err) {
      console.log('err = ', err)
    }
    */
    const profile: IProfile = await ProfileModel.addProfile(newProfile)

    const tokensHelper = new TokensHelper(
      Config.tokens.salt,
      Config.tokens.accessLifeTime,
      Config.tokens.refreshLifeTime
    )
    const { token, refreshToken } = tokensHelper.createTokens(
      profile.userID as string
    )
    const profileWithTokens: IProfile = await ProfileModel.updateTokensByID(
      profile.userID as string,
      token,
      refreshToken
    )

    return profileWithTokens
  }
}
