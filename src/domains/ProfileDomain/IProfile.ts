export default interface IProfile {
  userID?: string
  email: string | null
  phone: string | null
  password: string
  token: string
  refreshToken: string
}
