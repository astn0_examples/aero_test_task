/**
 * название, расширение, MIME type, размер, дата
 * загрузки;
 */
export default interface IFiles {
  fileID?: string
  name: string
  extension: string
  mime: string
  size: number
  uploadDate: Date
  path?: string
}
