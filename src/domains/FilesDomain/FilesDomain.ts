import IPagData from '../../libs/Pagination/IPagData'
import IPagination from '../../libs/Pagination/IPagination'
import Pagination from '../../libs/Pagination/Pagination'
import FileModel from '../../models/FileModel'
import IFiles from './IFiles'
import fs from 'fs'
import QueueService from '../../servises/QueueService'
import Config from '../../globals/Config'

/**
 * Домен для реализации логики работы с файлами
 */
export default class FilesDomain {
  private _fileMeta: IFiles | null = null

  constructor() {}

  public get fileMeta(): IFiles {
    return this._fileMeta as IFiles
  }

  /**
   * Загружаем информацию по всем файлам
   */
  public async getAllFiles(
    page: number,
    list_size: number
  ): Promise<IPagData<IFiles>> {
    const dataFromFiles: Array<FileModel> = await FileModel.findAll({})
    const files: Array<IFiles> = dataFromFiles.map((x) =>
      FileModel.mapToFile(x)
    )
    return Pagination.pagination(page, list_size, files)
  }
  /**
   * Загружаем информацию по одному файлу
   */
  public async getOneFile(fileID: string): Promise<void> {
    const dataFromFiles = (await FileModel.findOne({
      where: {
        id: fileID,
      },
    })) as FileModel
    this._fileMeta = FileModel.mapToFile(dataFromFiles)
  }
  /**
   * добавление нового файла в систему и запись
   * параметров файла в базу: название, расширение, MIME type, размер, дата
   * загрузки;
   * @param file
   */
  public async uploadOneFile(file: IFiles): Promise<void> {
    console.log(' информация о файле для добавления в базу данных = ', file)
    await FileModel.create({ ...file })
  }

  /**
   * Обновляем информацию о файле
   *
   * @param file
   */
  public async updateFile(file: IFiles): Promise<void> {
    const id = file.fileID as string
    delete file.fileID
    console.log(' информация о файле для добавления в базу данных = ', file)

    let dataFromFiles: any = await FileModel.findOne({
      where: {
        id,
      },
    })

    dataFromFiles.name = file.name
    dataFromFiles.extension = file.extension
    dataFromFiles.mime = file.mime
    dataFromFiles.size = file.size
    dataFromFiles.uploadDate = file.uploadDate

    await dataFromFiles.save()
    this._fileMeta = FileModel.mapToFile(dataFromFiles)
  }
  /**
   * Удаляем файл из бд и из хранилища
   *
   * @param fileID
   */
  public async deleteFile(fileID: string): Promise<void> {
    const dataFromFiles = (await FileModel.findOne({
      where: {
        id: fileID,
      },
    })) as FileModel
    this._fileMeta = FileModel.mapToFile(dataFromFiles)
    await dataFromFiles.destroy()
    console.log('удаляем файл = ', this._fileMeta.path + this._fileMeta.name)
    // todo: удаление в отдельный сервис (связь через rabbitMQ)
    /*
    fs.unlink('./uploads/' + this._fileMeta.path, (err) => {
      if (err) throw err
    })
    */
    QueueService.sendMessage('delete_file', this._fileMeta.path as string)
  }
}
