import { Request, Response, NextFunction } from 'express'
export default {
  ERROR: (
    error: any,
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    if (error.message === '404') {
      response.status(404).json({ code: 404 })
    } else if (error.message === '401') {
      response.status(401).json({ code: 401 })
    } else if (error.errors) {
      let errors: any = {}
      for (let err of error.errors) {
        errors[err.param] = err.msg
      }
      response.status(400).json({ ...errors })
    } else {
      response.status(520).json({
        message: 'Unknown Error',
      })
    }
  },
}
