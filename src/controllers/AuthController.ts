import { Request, Response, NextFunction, urlencoded } from 'express'
import { check, validationResult } from 'express-validator'
import IProfile from '../domains/ProfileDomain/IProfile'
import LoginTypeEnum from '../domains/ProfileDomain/LofinTypeEnum'
import ProfileDomain from '../domains/ProfileDomain/ProfileDomain'
import IRequest from './IRequest'
export default class AuthController {
  /**
   * Логинимся в систему, получаем токены
   * @param req
   * @param res
   */
  public static async signin(req: Request, res: Response): Promise<void> {
    await check('id').isString().run(req)
    await check('password').isString().run(req)
    validationResult(req).throw()

    const id: string = req.body.id
    const password: string = req.body.password
    let loginType: LoginTypeEnum

    // todo: убери в конфиг
    const reEmail = new RegExp(/^[\w\.\-\+]{1,30}@[\w\.\-]{1,30}\.[\w]{2,4}$/)
    const rePhone = new RegExp(/^\+7[\d]{5,20}$/)

    if (id.search(reEmail) !== -1) {
      loginType = LoginTypeEnum.email
    } else if (id.search(rePhone) !== -1) {
      loginType = LoginTypeEnum.phone
    } else {
      let error: any = new Error()
      error.errors = [{ param: 'id', msg: 'Не телефон, не мыло ' }]
      throw error
    }
    const profileDomain = new ProfileDomain()
    const profile: IProfile = await profileDomain
      .getProfileByLogin(id, password, loginType)
      .catch((err) => {
        throw Error('401')
      })
    res.json({
      data: {
        accessToken: profile.token,
        refreshToken: profile.refreshToken,
      },
    })
  }
  /**
   * Добавляем нового пользователя в систему
   * @param req
   * @param res
   */
  public static async signup(req: Request, res: Response): Promise<void> {
    await check('id').isString().run(req)
    await check('password').isString().run(req)
    validationResult(req).throw()

    const id: string = req.body.id
    const password: string = req.body.password
    let email: string | null = null
    let phone: string | null = null

    // todo: убери в конфиг
    const reEmail = new RegExp(/^[\w\.\-\+]{1,30}@[\w\.\-]{1,30}\.[\w]{2,4}$/)
    const rePhone = new RegExp(/^\+7[\d]{5,20}$/)

    if (id.search(reEmail) !== -1) {
      email = id
    } else if (id.search(rePhone) !== -1) {
      phone = id
    } else {
      let error: any = new Error()
      error.errors = [{ param: 'id', msg: 'Не телефон, не мыло ' }]
      throw error
    }

    const profileDomain = new ProfileDomain()
    const test = await profileDomain.addProfile({
      email,
      phone,
      password,
      token: '',
      refreshToken: '',
    })

    res.json({
      body: {
        success: true,
        test,
      },
    })
  }
  /**
   * Получаем новые токины по refresh токену
   * @param req
   * @param res
   */
  public static async doRefreshToken(
    req: Request,
    res: Response
  ): Promise<void> {
    await check('refreshToken').isString().run(req)
    validationResult(req).throw()

    const oldRefreshToken: string = req.body.refreshToken

    const profileDomain = new ProfileDomain()
    const profile: IProfile = await profileDomain
      .getProfileByRefreshToken(oldRefreshToken)
      .catch((err) => {
        throw Error('401')
      })

    res.json({
      data: {
        accessToken: profile.token,
        refreshToken: profile.refreshToken,
      },
    })
  }
  /**
   * Logout. Не понял, зачем в задании указано создать новые токены, а не удалить старые.
   * @param req
   * @param res
   */
  public static async logout(req: IRequest, res: Response): Promise<void> {
    const profileID: string = req.userID as string

    if (!profileID) throw '401'
    const profileDomain = new ProfileDomain()
    const profile: IProfile = await profileDomain.getProfileByProfileID(
      profileID
    )
    res.json({
      data: {
        accessToken: profile.token,
        refreshToken: profile.refreshToken,
      },
    })
  }
}
