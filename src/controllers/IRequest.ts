import { Request } from 'express'
type IRequest = Request & { userID?: string; files?: any }
export default IRequest
