import { Request, Response, NextFunction, urlencoded } from 'express'
import { check, validationResult } from 'express-validator'
import { noExtendLeft } from 'sequelize/types/lib/operators'
import FilesDomain from '../domains/FilesDomain/FilesDomain'
import IFiles from '../domains/FilesDomain/IFiles'
import FileMetaHelper from '../helpers/FileMetaHelper'
import IPagData from '../libs/Pagination/IPagData'
import IRequest from './IRequest'
export default class FilesController {
  /**
   * Получаем информацию обо всех файлах
   * @param req
   * @param res
   */
  public static async getAllFiles(req: IRequest, res: Response): Promise<void> {
    const page = req.query.page ? parseInt(req.query.page as string) : 1
    const list_size = req.query.list_size
      ? parseInt(req.query.list_size as string)
      : 10

    const filesDomain = new FilesDomain()
    const files: IPagData<IFiles> = await filesDomain.getAllFiles(
      page,
      list_size
    )

    res.json({
      data: {
        files,
      },
    })
  }
  /**
   * Получаем информацию об одном файле
   * @param req
   * @param res
   */
  public static async getOneFile(req: IRequest, res: Response): Promise<void> {
    const fileID: string = req.params.id
    const filesDomain = new FilesDomain()

    try {
      await filesDomain.getOneFile(fileID)
    } catch (err) {
      console.log('нет такого файла')
      throw new Error('404')
    }

    res.json({
      data: {
        file: filesDomain.fileMeta,
      },
    })
  }
  /**
   * Загрузка файла
   * @param req
   * @param res
   */
  public static async uploadOneFile(
    req: IRequest,
    res: Response
  ): Promise<void> {
    const fileHelper = new FileMetaHelper()
    const uploadedFile: IFiles = fileHelper.getFileMeta(req.files.file)

    await new Promise((resolve, reject) => {
      req.files.file.mv(`./uploads/${uploadedFile.name}`, (err: Error) => {
        console.log('upload error = ', err)
        resolve() // todo: бросай ошибку, если файл не загрузился
      })
    })

    const filesDomain = new FilesDomain()

    try {
      await filesDomain.uploadOneFile(uploadedFile)
    } catch (err) {
      console.log(err)
      throw new Error(err)
    }

    res.json({
      data: {
        success: true,
        uploadedFile,
      },
    })
  }
  /**
   * Обновляем файл
   * @param req
   * @param res
   */
  public static async updateOneFile(
    req: IRequest,
    res: Response
  ): Promise<void> {
    const fileID: string = req.params.id

    const fileHelper = new FileMetaHelper()
    let uploadedFile: IFiles = fileHelper.getFileMeta(req.files.file)
    uploadedFile.fileID = fileID

    const filesDomain = new FilesDomain()

    try {
      await filesDomain.updateFile(uploadedFile)
    } catch (err) {
      console.log('нет такого файла')
      throw new Error('404')
    }

    await new Promise((resolve, reject) => {
      req.files.file.mv(
        `./uploads/${filesDomain.fileMeta.name}`,
        (err: Error) => {
          console.log('upload error = ', err)
          resolve() // todo: бросай ошибку, если файл не загрузился
        }
      )
    })

    res.json({
      data: {
        success: true,
      },
    })
  }

  public static async deleteOneFile(
    req: IRequest,
    res: Response
  ): Promise<void> {
    const fileID: string = req.params.id

    const filesDomain = new FilesDomain()
    try {
      await filesDomain.deleteFile(fileID)
    } catch (err) {
      throw new Error('404')
    }
    // todo: delete file
    res.json({
      data: {
        success: true,
        file: filesDomain.fileMeta,
      },
    })
  }
  /**
   * Загружаем файл
   *
   * @param req
   * @param res
   */
  public static async downloadOneFile(
    req: IRequest,
    res: Response
  ): Promise<void> {
    const fileID: string = req.params.id
    const filesDomain = new FilesDomain()

    try {
      await filesDomain.getOneFile(fileID)
    } catch (err) {
      console.log('нет такого файла')
      throw new Error('404')
    }

    res.download(
      `./uploads/${filesDomain.fileMeta.name}`,
      filesDomain.fileMeta.name
    )
  }
}
