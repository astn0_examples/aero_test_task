import { DataTypes, Model } from 'sequelize'
import IFiles from '../domains/FilesDomain/IFiles'
import SequelizeSingleton from '../libs/SequelizeSinglton'

class FileModel extends Model {
  public static mapToFile(data: any): IFiles {
    return {
      fileID: data.id,
      name: data.name,
      extension: data.extension,
      mime: data.mime,
      size: data.size,
      uploadDate: data.uploadDate,
      path: data.path,
    }
  }
}

FileModel.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    extension: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    mime: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    size: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    uploadDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: SequelizeSingleton,
    modelName: 'files',
    tableName: 'files',
  }
)

FileModel.sync()
export default FileModel
