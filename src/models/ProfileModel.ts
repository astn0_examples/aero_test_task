import { DataTypes, Model } from 'sequelize'
import IProfile from '../domains/ProfileDomain/IProfile'
import SequelizeSingleton from '../libs/SequelizeSinglton'

class ProfileModel extends Model {
  public static async getProfileByEmail(email: string): Promise<IProfile> {
    const profile = await this.findOne({
      where: {
        email,
      },
    })
    return this._mapToProfile(profile)
  }
  public static async getProfileByPhone(phone: string): Promise<IProfile> {
    const profile = await this.findOne({
      where: {
        phone,
      },
    })
    return this._mapToProfile(profile)
  }

  /**
   * Добавляем нового пользователя в систему
   * @param profile
   */
  public static async addProfile(profile: IProfile): Promise<IProfile> {
    const createdProfile = await this.create({ ...profile })
    console.log('createdProfile = ', createdProfile)
    return this._mapToProfile(createdProfile)
  }
  /**
   * Обновляем токены для ID пользователя
   * @param profileID
   * @param accessToken
   * @param refreshToken
   */
  public static async updateTokensByID(
    profileID: string,
    accessToken: string,
    refreshToken: string
  ): Promise<IProfile> {
    await this.update(
      {
        token: accessToken,
        refreshToken: refreshToken,
      },
      {
        where: {
          id: profileID,
        },
      }
    )
    const result = await this.findOne({
      where: {
        id: profileID,
      },
    })
    return this._mapToProfile(result)
  }

  public static async getProfileByToken(token: string): Promise<IProfile> {
    const profile = await this.findOne({
      where: {
        token,
      },
    })
    return this._mapToProfile(profile)
  }
  public static async getProfileByRefreshToken(
    refreshToken: string
  ): Promise<IProfile> {
    const profile = await this.findOne({
      where: {
        refreshToken,
      },
    })
    return this._mapToProfile(profile)
  }

  private static _mapToProfile(data: any): IProfile {
    return {
      userID: data.id,
      email: data.email,
      phone: data.phone,
      password: data.password,
      token: data.token,
      refreshToken: data.refreshToken,
    }
  }
}

ProfileModel.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    refreshToken: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: SequelizeSingleton,
    modelName: 'profiles',
    tableName: 'profiles',
  }
)
ProfileModel.sync()
export default ProfileModel
